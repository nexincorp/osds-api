<?php

namespace Osds\Api\Framework\Symfony;

use Osds\Api\Infrastructure\Controllers\BaseController;

use Illuminate\Http\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Psr\Log\LoggerInterface;
use App\Util\Aws\AwsS3Util;
use App\Util\Aws\AwsSnsUtil;
use App\Service\Mailer\MailClients\SimpleEmailServiceClient;
use App\Application\Commands\Email\SendVoucherEmailCommand;

/**
 * @Route("/api/{model}")
 */
class SymfonyController extends BaseController {


    public function __construct(
        Request $request,
        EntityManagerInterface $entity_manager,
        LoggerInterface $logger,
        AwsS3Util $awss3util,
        SimpleEmailServiceClient $awsSes,
        AwsSnsUtil $awsSns
    )
    {
        $this->services = [
            'entity_manager' => $entity_manager,
            'logger' => $logger,
            'awss3util' => $awss3util,
            'awsSes' => $awsSes,
            'awsSns' => $awsSns
        ];

        $_SESSION['services'] = $this->services;

        parent::__construct($request);
    }

    /**
     * @Route(
     *     "/{id?}",
     *     methods={"POST"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function upsert($model, $id = null)
    {
        return $this->handle(__FUNCTION__, $model, $id);
    }

    /**
     * @Route(
     *     "/schema",
     *     methods={"GET"}
     * )
     */
    public function getSchema($model, $id = null)
    {
        return $this->handle(__FUNCTION__, $model, $id);
    }

    /**
     * @Route(
     *     "/getMetadata",
     *     methods={"GET"}
     * )
     */
    public function getMetadata($model, $id = null)
    {
        return $this->handle(__FUNCTION__, $model, $id);
    }

    
    /**
     * @Route(
     *     "/{id?}",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function get($model, $id = null)
    {
        return $this->handle(__FUNCTION__, $model, $id);
    }

    /**
     * @Route(
     *     "/{id}",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function delete($model, $id = null)
    {
        return $this->handle(__FUNCTION__, $model, $id);
    }

    private function handle($action, $model, $id)
    {
        $uri_params[] = $model;
        $uri_params[] = $id;
        return parent::__call($action, $uri_params);
    }

    public function generateResponse($data, $action = null)
    {
        #assigned to have it on the destruct()
        $this->response = $data;
        // All actions except 'get' do not return 'items schema' => Generate it!!
        if($action != 'get'){
            return JsonResponse::fromJsonString(json_encode($this->prepareResponseByItems($data)));
        } else {
            return JsonResponse::fromJsonString(json_encode($data));
        }
    }

}