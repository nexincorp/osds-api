<?php

namespace Osds\Api\Framework\Laravel;

use Osds\Api\Infrastructure\Controllers\BaseController;

class LaravelController extends BaseController {

    public function generateResponse($data, $action = null)
    {
        #assigned to have it on the destruct()
        $this->response = $data;
        // All actions except 'get' do not return 'items schema' => Generate it!!
        if($action != 'get'){
            return response()->json($this->prepareResponseByItems($data), 200);
        } else {
            return response()->json($data, 200);
        }
    }


}