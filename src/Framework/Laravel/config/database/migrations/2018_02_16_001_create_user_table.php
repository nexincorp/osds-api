<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64)->nullable();
            $table->string('email', 64)->nullable();
            $table->string('password', 64)->nullable();
            $table->text('description')->nullable();
            $table->text('extra_data')->nullable();
            $table->string('acl', 6)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('user')->insert(
            array(
                'name' => 'Admin',
                'email' => 'admin@nexin.es',
                'password' => bcrypt('masterNexinPasswordJust1NC#se'),
                'description' => 'Nexin user for accesing the Backoffice',
                'extra_data' => '',
                'acl' => '111111'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
