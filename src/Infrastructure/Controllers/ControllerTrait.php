<?php

namespace Osds\Api\Infrastructure\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;

trait ControllerTrait
{

    private $commands_ns = '\Osds\Api\Application\Commands\%action%ModelCommand';

    public function callCommand($action)
    {
        try
        {
            #get command location
            $command_location = $this->getCommandLocation($action);
            #execute loaded command

            $command = new $command_location($this->model, $this->args);
            $res = $command->execute();
        } catch(\Exception $e)
        {

            $code = ($e->getCode() == 0) ? 400 : $e->getCode();

            $res =
                ['error' => [
                    'code' => $code,
                    'messages' => $e->getMessage()
                ]];

            $response = new JsonResponse($res, $code);
            $response->setStatusCode($code);
            $response->send();

        }
        return $res;
    }

    /**
     * Returns the fully qualified name of the command to execute
     *
     * @param $action
     * @return mixed
     */
    private function getCommandLocation($action)
    {
        #does this model have a custom command?
        $model_command = 'App\Application\Commands\\' . ucfirst($this->model) . '\\' . ucfirst($action) . ucfirst($this->model) . 'Command';
        if(class_exists($model_command))
        {
            return $model_command;
        }

        #return generic command
        return str_replace(
            '%action%',
            ucfirst($action),
            $this->commands_ns
        );
    }


    public function prepareResponseByItems($data)
    {
        $items[] = $data;
        $num_items = ($data != true)? 0: count($items);

        return [
            'total_items' => $num_items,
            'items' => $items
        ];
    }

}