<?php

namespace Osds\Api\Infrastructure\Controllers;

use Illuminate\Http\Request;



abstract class BaseController
{

    use ControllerTrait;

    private $tokens = ['PublicTokenForRequestingAPI'];
    public $args;
    protected $logger;
    protected $awsSes;
    protected $awsSns;

    public function __construct(Request $request) {
        $api_token = @$request->header('X-Auth-Token');
        if (
            is_null($api_token)
            || !in_array($api_token, $this->tokens)
        ) {
//            die('Invalid token');
        }

        $this->args = $_REQUEST;

        if (!empty($_FILES)) {
            $this->args['files'] = $_FILES;
        }
    }

    /**
     * @param $action           name of the function that is called (same as the command first part ($actionModelCommand)
     * @param $uri_params
     * @return array            response of the command
     */
    public function __call($action, $uri_params)
    {
        #first param of url is always the model
        $this->model = ucfirst(array_shift($uri_params));
        // we store the extra params of the url
        $this->args = array_merge($this->args, ['uri_params' => $uri_params]);
        #call the command
        return $this->generateResponse($this->callCommand($action), $action);
    }


    public function __destruct()
    {
        if(class_exists('\App\Events\PostExecution'))
        {
            $middleware = new \App\Events\PostExecution();

            if(isset($_REQUEST['log_event'])) {

                if(isset($this->response['upsert_id']))
                {
                    $id = $this->response['upsert_id'];
                } else if(isset($this->model_id)) {
                    $id = $this->model_id;
                } else {
                    $id = 0;
                }

                if(isset($_REQUEST['user_id']))
                {
                    $user_id = $_REQUEST['user_id'];
                } else {
                    $user_id = 1;
                }

                unset($this->args['log_event']);

                $middleware->logEvent($_REQUEST['log_event'], $this->model, $id, $this->args, $user_id);
            }

        }
    }




}