<?php

namespace Osds\Api\Infrastructure\Repositories;

use Doctrine\DBAL\Query\QueryBuilder;
use Osds\Api\Domain\Entity\BaseEntity;
use Osds\Api\Framework\Symfony\FakeReflectionFactory;

use Doctrine\ORM\Query;

class DoctrineModelRepository implements ApiRepository
{
    public function __construct()
    {
        $this->entity_manager = $_SESSION['services']['entity_manager'];
    }

    public function setEntity($entity)
    {
        $this->setModel($entity);
        $this->setTable($entity);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        if(strstr($model, '_'))
        {
            $this->model = $this->underscoreToCamelCase($model);
        } else {
            $this->model = ucfirst($model);
        }
    }

    public function getTable()
    {
        return $this->table;
    }

    public function setTable($model)
    {
        $this->table = $this->camelCaseToUnderscore($model);
    }


    public function getModelFields($model)
    {
        $sql = 'SHOW columns FROM ' . $this->camelCaseToUnderscore($model);
        $result = $this->executeStatement($sql);

        $columns = [];
        foreach($result as $r)
        {
            $columns[$r['Field']] = $r;
        }
        return $columns;
    }

    public function getReferencesWithOtherModels($model) {

        $sql = 'SELECT 
                      `TABLE_SCHEMA`,                          -- Foreign key schema
                      `TABLE_NAME`,                            -- Foreign key table
                      `COLUMN_NAME`,                           -- Foreign key column
                      `REFERENCED_TABLE_SCHEMA`,               -- Origin key schema
                      `REFERENCED_TABLE_NAME`,                 -- Origin key table
                      `REFERENCED_COLUMN_NAME`                 -- Origin key column
                    FROM
                      `INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`  -- Will fail if user dont have privilege
                    WHERE
                      `TABLE_SCHEMA` = SCHEMA()                -- Detect current schema in USE 
                      AND `REFERENCED_TABLE_NAME` IS NOT NULL 
                      AND (`TABLE_NAME` = "' . $this->camelCaseToUnderscore($model) . '" OR `REFERENCED_TABLE_NAME` = "' . $this->camelCaseToUnderscore($model) . '") 
                    ';
        $results = $this->executeStatement($sql);
        if(count($results))
        {
            foreach($results as &$result)
            {
                $result = json_decode(json_encode($result));
            }
            return $results;
        } else {
            return [];
        }

    }

    #TODO: "$query_builder->from" doesn't work
    public function retrieve($model, Array $search_fields = null, Array $query_filters = null) {

        $this->setEntity($model);

        $joined_models = [];

        #get repository and query builder for the queries
        $repository = $this->entity_manager->getRepository('\App\Entity\\' . $this->getModel());
        /** @var QueryBuilder $query_builder */
        $query_builder = $repository->createQueryBuilder($model);

        if($search_fields != null) {
            foreach($search_fields as $field_name => $props)
            {
                #model to use on the where clause
                $where_model = $model;
                $this->leftJoinQueryBuilder($model, $field_name, $joined_models, $query_builder, $where_model);

                #looking for an exact match of anything else
                if(is_array($props)) {
                    if(empty($props['value']) && $props['value'] !== "0") continue;
                    $value = $props['value'];
                    if(isset($props['operand'])) {
                        $operand = $props['operand'];
                        if($operand == 'LIKE') {
                            $value = "%{$value}%";
                        }
                    } else {
                        $operand = 'LIKE';
                        $value = "%{$value}%";
                    }
                } else {
                    if(empty($props)) continue;
                    $value = $props;
                    $operand = '=';
                }

                $query_builder->andWhere("{$where_model}.{$field_name} {$operand} '{$value}'");

            }
        }

        if($query_filters != null) {
            if(isset($query_filters['sortby']))
            {
                for ($i=0; $i<count($query_filters['sortby']); $i++) {
                    $field_name = $query_filters['sortby'][$i]['field'];
                    $order_model = $model;
                    $this->leftJoinQueryBuilder($model, $field_name, $joined_models, $query_builder, $order_model);
                    $query_builder->addOrderBy($order_model . '.' . $field_name, $query_filters['sortby'][$i]['dir']);
                }
            }
        }

        if(isset($query_filters['page_items']))
        {
            if(!isset($query_filters['page']))
            {
                $query_filters['page'] = 1;
            }

            $start = ($query_filters['page'] - 1) * $query_filters['page_items'];

            $query_builder_total = clone $query_builder;
            $query_builder_total->select("count({$model}.id)");
            $total_items = $query_builder_total->getQuery()->getSingleScalarResult();

            $query_builder->setFirstResult($start)->setMaxResults($query_filters['page_items']);

        }

        $results = $query_builder->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $items = [];
        foreach ($results as $r)
        {
            array_walk($r, array('self', 'fieldDateTimeObjectToString'));
            $items[] = $r;
        }


        return [
            'total_items' => isset($total_items)?$total_items:count($items),
            'items' => $items
        ];

    }

    public function upsert($model_id, $data): int
    {
        $entity = 'App\Entity\\' . $this->getModel();

        if($model_id == null) {
            $repository = new $entity();
        } else {
            $repository = $this->entity_manager->getRepository($entity)->find($model_id);
        }

        #treat fields before updating / inserting
        foreach($data as $field => $value)
        {
            #if matches a yyyy-mm-dd, yyyy-mm-dd hh:ii, or yyyy-mm-dd hh:ii:ss
            if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}(:[0-9]{2})?)?$/', $value))
            {
                #add seconds to allow this type of date (yyyy-mm-dd hh:ii)
                if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$/', $value))
                {
                    $value .= ':00';
                }
                $value = new \DateTime($value);
            }
            $repository->{$field} = $value;
        }

        if($model_id == null) {
            $this->entity_manager->persist($repository);
            $result = $this->entity_manager->flush();
            return $repository->getId();
        } else {
            $this->entity_manager->merge($repository);
            $result = $this->entity_manager->flush();
            return $model_id;
        }

    }

    public function remove($model_id) {

//        $this->entity_manager->getFilters()->enable('softdeleteable');

        $entity = 'App\Entity\\' . $this->getModel();
        $object = $this->entity_manager->getRepository($entity)->find($model_id);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $this->entity_manager->remove($object);

        // actually executes the queries (i.e. the INSERT query)
        $this->entity_manager->flush();

        return $model_id;
    }

    public function executeStatement($sql)
    {
        $stmt = $this->entity_manager->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function underscoreToCamelCase($input)
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
    }

    public function camelCaseToUnderscore($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    private function fieldDateTimeObjectToString(&$result)
    {
        if ($result instanceof \DateTime) {
            $result = $result->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param $model
     * @param $field_name
     * @param $joined_models
     * @param $query_builder
     * @param $order_model
     */
    private function leftJoinQueryBuilder($base_model, &$field_name, &$joined_models, $query_builder, &$related_model): void
    {
        if (strstr($field_name, '.')) {
            [$related_model, $field_name] = explode('.', $field_name);
            $related_model_class = '\App\Entity\\' . ucfirst($related_model);
            if (!in_array($related_model, $joined_models)) {
                $joined_models[] = $related_model;
                $query_builder->leftJoin(
                    $related_model_class,
                    $related_model,
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    "{$related_model}.id = {$base_model}.{$related_model}_id"
                );
            }
        }
    }
}
