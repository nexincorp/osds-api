<?php

namespace Osds\Api\Infrastructure\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EloquentModelRepository extends Model implements ApiRepository  {


    public function setEntity($entity)
    {
        $this->setModel($entity);
        $this->setTable($entity);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        if(strstr($model, '_'))
        {
            $this->model = $this->underscoreToCamelCase($model);
        } else {
            $this->model = ucfirst($model);
        }
    }

    public function getTable()
    {
        return $this->table;
    }

    public function setTable($model)
    {
        $this->table = $this->camelCaseToUnderscore($model);
    }

    public function getModelFields($model)
    {
        $result = DB::select('SHOW columns FROM ' . $this->getTable());
        $columns = [];
        foreach($result as $r)
        {
            $columns[$r->Field] = $r;
        }
        return $columns;
    }

    public function getReferencesWithOtherModels($model)
    {
        return DB::select('SELECT 
                                  `TABLE_SCHEMA`,                          -- Foreign key schema
                                  `TABLE_NAME`,                            -- Foreign key table
                                  `COLUMN_NAME`,                           -- Foreign key column
                                  `REFERENCED_TABLE_SCHEMA`,               -- Origin key schema
                                  `REFERENCED_TABLE_NAME`,                 -- Origin key table
                                  `REFERENCED_COLUMN_NAME`                 -- Origin key column
                                FROM
                                  `INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`  -- Will fail if user dont have privilege
                                WHERE
                                  `TABLE_SCHEMA` = SCHEMA()                -- Detect current schema in USE 
                                  AND `REFERENCED_TABLE_NAME` IS NOT NULL 
                                  AND (`TABLE_NAME` = "' . $model . '" OR `REFERENCED_TABLE_NAME` = "' . $model . '") 
                    ');
    }

    public function retrieve($model, Array $search_fields = null, Array $query_filters = null) {

        $object = $this->guard([]);
        $object->setTable($model);

        if($search_fields != null) {
            foreach($search_fields as $field_name => $props)
            {
                #looking for an exact match of anything else
                if(is_array($props)) {
                    if(empty($props['value'])) continue;
                    $value = $props['value'];
                    if(isset($props['operand'])) {
                        $operand = $props['operand'];
                        if($operand == 'LIKE') {
                            $value = "%{$value}%";
                        }
                    } else {
                        $operand = 'LIKE';
                        $value = "%{$value}%";
                    }
                } else {
                    if(empty($props)) continue;
                    $value = $props;
                    $operand = '=';
                }

                $object = $object->where($field_name, $operand, $value);
            }
        }

        $total_items = $object->count();

        if($query_filters != null) {
            if(isset($query_filters['sortby']))
            {
                for ($i=0; $i<count($query_filters['sortby']); $i++) {
                    $object = $object->orderBy($query_filters['sortby'][$i]['field'], $query_filters['order'][$i]['dir']);
                }
            }
            if(isset($query_filters['page_items']))
            {
                if(!isset($query_filters['page']))
                {
                    $query_filters['page'] = 1;
                }

                $start = ($query_filters['page'] - 1) * $query_filters['page_items'];

                $object = $object->take($query_filters['page_items'])->offset($start);

            }
        }

        $results = [];
        $results = $object->get()->toArray();

        return [
            'total_items' => $total_items,
            'items' => $results
        ];
    }

    public function upsert($id, $data)
    {
        if($id != null)
        {
            #load eloquent model
            #set the table for the generic model
            $object = $this;
            $object->setTable($this->getModel());

            #load entry and fill with new data
            $entry = $object->find($id);
            $entry->guard([]);
            $entry->fill($data);
            #set the table again (it gets lost)
            $entry->setTable($this->getModel());
            $result = $entry->save();

        } else {
            $entry = $this;
            $entry->setTable($this->getModel());
            foreach($data as $k => $v)
            {
                $entry->{$k} = $v;
            }
            unset($entry->model);
            $result = $entry->save();
        }
        if($result)
        {
            return $entry->id;
        } else {
            return null;
        }
    }

    public function remove($id)
    {
        $object = $this->guard([]);
        $object->setTable($this->getModel());

        $entry = $object->find($id);
        $entry->setTable($this->getModel());
        if($entry->delete()) {
            return $id;
        } else {
            return null;
        }
    }

    public function underscoreToCamelCase($input)
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
    }

    public function camelCaseToUnderscore($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    private function fieldDateTimeObjectToString(&$result)
    {
        if ($result instanceof \DateTime) {
            $result = $result->format('Y-m-d H:i:s');
        }
    }

}