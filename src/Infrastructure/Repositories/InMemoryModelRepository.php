<?php

namespace Osds\Api\Infrastructure\Repositories;

class InMemoryModelRepository implements ApiRepository
{
    public function setModel($model) {}

    public function getModel() {}

    public function getModelFields($model) {}

    public function getReferencesWithOtherModels($model) {}

    public function retrieve($model, Array $search_fields, Array $query_filters) {}

    public function upsert($model_id, $data) {}

    public function remove($model_id) {}

}
