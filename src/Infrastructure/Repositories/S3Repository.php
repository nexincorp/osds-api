<?php

namespace Osds\Api\Infrastructure\Repositories;

class S3Repository
{

    public static function persist($aws3_service, $filename, $content, $parameters)
    {
        try {
            $tmp_file = '/tmp/' . $filename;
            file_put_contents($tmp_file, $content);
            $s3_url = $aws3_service->put(
                getenv('AWS_IMAGE_BUCKET'),
                $tmp_file,
                $parameters['folder']);
            return $s3_url;
        } catch(\Exception $e)
        {
            return ($e->getMessage());
        }
    }

}