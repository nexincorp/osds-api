<?php

/**
 * Gets the db fields for this model and their possible relations (searched by db foreign keys) with other models
 */
namespace Osds\Api\Application\Commands;

class GetMetadataModelCommand extends BaseCommand
{

    /**
     *
     * @return array
     */
    public function execute($model = null)
    {
        if($model != null)
        {
            $this->repository->setEntity($model);
        }

        $data = [];

        #Get Model possible statuses
        $model_statuses = [];
        $entity = 'App\Entity\\' . $this->repository->getModel();
        if(class_exists($entity))
        {
            $helper_class = new \ReflectionClass($entity);
            $class_constants = $helper_class->getConstants();

            $model_statuses = [];
            foreach($class_constants as $k => $v)
            {
                if(strstr($k, "STATUS_")) {
                    $model_statuses[$v] = ucfirst(strtolower(str_replace("_", " ", str_replace("STATUS_", "", $k))));
                }
            }
        }


        $model_data = [
            'status' => $model_statuses,
            'status_by_name' => array_flip($model_statuses)
        ];

        $debug_backtrace = debug_backtrace();
        if(
            #check it's not recursive
            isset($debug_backtrace[1])
            &&
            isset($debug_backtrace[1]['class'])
            &&
            $debug_backtrace[1]['class'] != self::class
        )
        {
            $data[strtolower($this->repository->getModel())] = $model_data;

            if(isset($this->args['related_models']))
            {
                $related_models = explode(',', $this->args['related_models']);
                foreach($related_models as $rm)
                {
                    $data[strtolower($rm)] = $this->execute($rm);
                }
            }
            return $data;

        } else {
            #it's rercursive => return only this data
            return $model_data;
        }

    }

}