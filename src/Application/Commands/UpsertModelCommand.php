<?php

/**
 * updates or inserts on db the data
 */
namespace Osds\Api\Application\Commands;

use Osds\Api\Infrastructure\Repositories\S3Repository;

class UpsertModelCommand extends BaseCommand
{

    public function execute()
    {
        $args = $this->args;
        unset($args['uri_params']);

        foreach($args as $key => $value) {
            #we want to avoid filtering for this field (maybe empty)
            if($value === 'DB_NULL') {
                $args[$key] = null;
            }

            if($key === 'files')
            {
                foreach ($value as $file => $file_data) {

                    // Check if Key has '##' => then it is a file that needs persistence
                    if (preg_match('/(.*)##(.*)\|(.*)$/', $file, $res)) {

                        $field_name = $res[1];
                        $repository = $res[2];
                        $path_folder = $res[3];

                        try {
                            $persisted = false;
                            switch ($repository) {
                                case 'S3':
                                    $args[$field_name] = S3Repository::persist($this->services['awss3util'], $file_data['name'], file_get_contents($file_data['tmp_name']), ['folder' => $path_folder]);
                                    $persisted = true;
                                    break;
                            }

                            if (!$persisted) {
                                throw new \Exception('No persistence valid method specified for ' . $file . ' (' . json_encode($file_data) . ')');
                            }
                        } catch (\Exception $e) {
                            return ['error_message' => $e->getMessage()];
                        }
                    }
                }

                unset($args['files']);
            }
            else if (is_array($value))
            {
                $args[$key] = json_encode($value);
            }
        }

        $this->model_id = $this->repository->upsert($this->model_id, $args);

        return [
            'upsert_id' => $this->model_id
        ];
    }

}