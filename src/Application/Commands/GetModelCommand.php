<?php

/**
 *
 * Command that obtains data from a request with possible params
 *
 */


namespace Osds\Api\Application\Commands;

class GetModelCommand extends BaseCommand
{

    /**
     * @param null $model :         if not set, the one from the request will be used
     * @param null $search_fields
     *             $search_fields[$fieldname] = value_to_search (performs a strict search (=));
     *             $search_fields[$fieldname] = [
     *                                           'value' => value_to_search,
     *                                           'operand' => 'LIKE' (only one supported by now)
     *
     * @param null $query_filters
     *             No sorting by default
     *             $query_filters['sortby'][$i]['field'] = $field_to_sort_by
     *             $query_filters['sortby'][$i]['dir'] = 'ASC', 'DESC'
     *
     *             $query_filters['page_items'] = n; number of results to retrieve
     *             $query_filters['page'] = i; pagination. from what index we want to start returning from (LIMIT $page_items, $page -1 * $page_items)
     *
     * bool get_referenced  :   gets the referenced contents (foreign contents) for each content of the result
     *
     * array get_models_contents : models that we want to get all their elements
     *
     * @return array : total_items : total number of elements (with no pagination)
     *                 items : items got
     *                 schema : schema (db fields) of the model requested
     */
    public function execute($model = null, $search_fields = null, $query_filters = null)
    {
        $model_schema = [];

        #if no model is received, used the one on the request
        if($model == null)
        {
            $model = $this->repository->getModel();
        } else {
            $this->repository->setEntity($model);
        }

        #if we have received an id, set the search fields to look for it
        if(isset($this->model_id)) {
            #we are filtering by an entry
            if (!is_array($search_fields)) $search_fields = [];
            $search_fields['id'] = $this->model_id;
        }

        #obtain the schema (db fields and possible foreign models) for this model
        $command = new GetSchemaModelCommand($model);
        $model_schema = $command->execute(isset($this->args['get_referenced']));

        #we are filtering by something
        if(isset($this->args['search_fields'])) {
            if(!isset($search_fields)) $search_fields = [];
            $search_fields += $this->args['search_fields'];
            #we don't need them anymore
            unset($this->args['search_fields']);
        }

        #we are filtering the result query
        if(isset($this->args['query_filters'])) {
            if(!isset($query_filters)) $query_filters = [];
            $query_filters += $this->args['query_filters'];
            #we don't need them anymore
            unset($this->args['query_filters']);
        }

        #retrieve the data from the database, using the specified repository
        $result_data = $this->repository->retrieve(
            $model,
            $search_fields,
            $query_filters
            );

        #we don't want to filter by this ID anymore
        unset($this->model_id);

        $debug_backtrace = debug_backtrace();
        if(
            isset($debug_backtrace[1])
            &&
            isset($debug_backtrace[1]['class'])
            &&
            $debug_backtrace[1]['class'] != self::class) #it's not recursive
        {
            $is_recursive = false;
            #only get this data for the main model requested
            $result_data['schema'] = $model_schema;
        } else {
            $is_recursive = true;
        }


        if(
            #we have items
            count($result_data['items']) > 0
            #we want to get referenced models
            && isset($this->args['get_referenced'])
            #but we are not in a depth of > 3 levels from the requested model (avoid too long nesting)
            && !(
                isset($debug_backtrace[2])
                &&
                isset($debug_backtrace[2]['class'])
                &&
                $debug_backtrace[2]['class'] == self::class
                )
            )
        {
            if(
                isset($this->args['get_referenced'])
                && isset($model_schema['referenced_models']) #model has foreignmodels
                && count($model_schema['referenced_models']) > 0)
            {
                #request possible referenced contents for each of the results
                foreach($result_data['items'] as &$r)
                {
                    #for each of the foreign models, look for their possible items
                    foreach($model_schema['referenced_models'] as $referenced_model => $referenced_info)
                    {
                        if(
                            #we don't want to get more in depth (it will return values we are not interesed in) if
                                #we are on recursive  do not get them
                            (   $is_recursive
                                #and the referenced model is the one that has a foreign key
                                && $referenced_info['referenced_model_has_foreign_key']
                            )
                            #or the key to search in is not set (it will return all of them)
                            || $r[$referenced_info['referenced_data']['model_field']] == null
                        )
                            continue;

                        $search_fields = [];
                        #we have to search, on the table of the foreign field, in the referenced_model_field, the value of the "id" of the current item
                        $model_field_to_search_in = $referenced_info['referenced_data']['referenced_model_field'];
                        $search_fields[$model_field_to_search_in] = $r[$referenced_info['referenced_data']['model_field']];
                        $r['relations'][$referenced_model] = self::execute(
                            #foreign table
                            $referenced_info['referenced_data']['referenced_model_table'],
                            #what to search for (the current item id on the foreign table field)
                            $search_fields);
                    }
                }
                #TODO REFACTOR: do not hardcore "note" model
                if(!$is_recursive)
                {
//                    $r['relations']['note'] = self::execute('note', ['related_model_id' =>  $r['id']]);
                }
            }

            if(
                !$is_recursive #only for base model
                && isset($this->args['get_models_contents']) #we want to get all the contents for this models (for example, list of referenced contents on Backoffice detail)
            )
            {
                unset($this->args['get_referenced']);
                foreach($this->args['get_models_contents'] as $model)
                {
                    $result_data['required_models_contents'][$model] = self::execute($model);
                }
            }

        }

        return $result_data;
    }

}