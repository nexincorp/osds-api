<?php

/**
 * Base class for Commands that does basic stuff (normalize request params and set a repository)
 */
namespace Osds\Api\Application\Commands;

use App\Application\Helpers;
use Osds\Api\Infrastructure\Repositories\ApiRepository;
use Osds\Api\Infrastructure\Repositories\DoctrineModelRepository;
use Osds\Api\Infrastructure\Repositories\EloquentModelRepository;

class BaseCommand
{
    protected $repository;

    public $model;

    public $args;
    
    public $services;

    public $model_id = null;

    protected $appModelName;

    /**
     * BaseCommand constructor.
     * @param null $repository_model : entity that is going to be treated
     * @param null $args : get / post arguments. If uri has an id, it's assigned to $this->model_id
     */
    public function __construct($repository_model = null, $args = null) {

        $this->repository = $this->getRepository();

        if($repository_model != null)
        {
            $this->repository->setEntity($repository_model);

            #transforms to ucfirst model recieved in the url. Used to handle custom commands
            $this->appModelName = $this->getAppModelName($repository_model);
        }

        $this->args = $args;

        if(class_exists('\App\Application\Helpers'))
        {
            $this->services = Helpers::getRequiredServices();
        }

        // We are requesting something from a determinated db entry. Save the id
        if(
            isset($this->args['uri_params'])
            && isset($this->args['uri_params'][0])
            && is_numeric($this->args['uri_params'][0])
        )
        {
            // Id comes from URL
            $this->model_id = array_shift($this->args['uri_params']);

        } else if (isset($this->args['id'])) {
            // Id comes from Internal API Requests
            $this->model_id = $this->args['id'];
        }

    }

    /**
     * Gets the repository to use
     *
     * @param $repository
     * @return DoctrineModelRepository|EloquentModelRepository
     */
    private function getRepository()
    {

        if(class_exists('App\Http\Controllers\Controller'))
        {
            $repository = new EloquentModelRepository();
        } else {
            $repository = new DoctrineModelRepository();
        }

        return $repository;
    }

    /**
     * generates a ucfirst-format name for the model requested (not in use currently)
     *
     * @param $model
     * @return string
     */
    private function getAppModelName($model)
    {
        $model_name_parts = explode('_', $model);
        $appModelName = '';
        while($part = array_shift($model_name_parts) != null)
        {
            $appModelName .= ucfirst($part);
        }
        return $appModelName;
    }

}