<?php

/**
 * Gets the db fields for this model and their possible relations (searched by db foreign keys) with other models
 */
namespace Osds\Api\Application\Commands;

class GetSchemaModelCommand extends BaseCommand
{

    /**
     * @param null $model
     * @param bool $get_foreign_models : in order to avoid recursivity, only get foreign models when it's the first caller
     * @return array
     */
    public function execute($get_foreign_models = false)
    {
        $model = $this->repository->getModel();

        $data = [];
        $data['fields'] = $this->repository->getModelFields($model);

        if($get_foreign_models)
        {
            $fkeys = $this->repository->getReferencesWithOtherModels($model);

            if(count($fkeys) > 0)
            {
                #model has foreign models
                $data['referenced_models'] = [];
                foreach($fkeys as $fk)
                {
                    if(strtolower($fk->TABLE_NAME) == $this->repository->camelCaseToUnderscore($model))
                    {
                        #the model that is requesting its schema is the one that has the foreign key (othermodel_id) on its table pointing to the main key on the other model
                        $data['referenced_models'][$fk->REFERENCED_TABLE_NAME]['referenced_data']['model_field'] = $fk->COLUMN_NAME;
                        $data['referenced_models'][$fk->REFERENCED_TABLE_NAME]['referenced_data']['referenced_model_table'] = $fk->REFERENCED_TABLE_NAME;
                        $data['referenced_models'][$fk->REFERENCED_TABLE_NAME]['referenced_data']['referenced_model_field'] = $fk->REFERENCED_COLUMN_NAME;
                        $data['referenced_models'][$fk->REFERENCED_TABLE_NAME]['referenced_model_has_foreign_key'] = false;
                    } else {
                        #the model requested is the "referenced" on the relationship
                        $data['referenced_models'][$fk->TABLE_NAME]['referenced_data']['model_field'] = $fk->REFERENCED_COLUMN_NAME;
                        $data['referenced_models'][$fk->TABLE_NAME]['referenced_data']['referenced_model_table'] = $fk->TABLE_NAME;
                        $data['referenced_models'][$fk->TABLE_NAME]['referenced_data']['referenced_model_field'] = $fk->COLUMN_NAME;
                        $data['referenced_models'][$fk->TABLE_NAME]['referenced_model_has_foreign_key'] = true;
                    }
                }
            }

            #recover schema data from the foreign fields (used for example to create an alias (table_field) on the db queries)
            if(isset($data['referenced_models'])) {
                foreach($data['referenced_models'] as &$fm)
                {
                    $this->repository->setModel($fm['referenced_data']['referenced_model_table']);
                    $fm['schema'] = $this->execute();
                }
            }
        }
        return $data;
    }

}