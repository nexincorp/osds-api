<?php namespace Osds\Api\Domain\Exception;

use InvalidArgumentException;

class ErrorException extends InvalidArgumentException {}


