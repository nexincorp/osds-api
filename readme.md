Install on Symfony
-------------------

composer require osds/api

add 
  "autoload": {
    "psr-4": {
      "App\\": "src/",
      "Osds\\Api\\": "vendor/osds/api/src"
    }
  }

to /composer.json

copy content of src/Framework/Symfony/config/.env.example to /.env

copy src/Framework/Symfony/config/osds_api.yaml to config/packages/osds_api.yaml

on config/routes.yaml, add
osds_api:
    resource: '../../vendor/osds/api/src/Framework/Symfony/'
    type:     annotation

---

Laravel

add to .env (on project requesting the API):
API_URL=http://domain.example/api/



---
Basic API system to handle basic CRUD operations on a Laravel - Mysql Server

* Relations are created from the mysql database foreign keys

pending
-------
cache schemas
soft deletes on symfony